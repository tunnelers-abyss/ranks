-- ranks/ranks.lua

ranks.register("admin", {
	prefix = "Admin",
	colour = {a = 255, r = 30, g = 250, b = 23},
	babybox = false,
})

ranks.register("moderator", {
	prefix = "Mod",
	colour = {a = 255, r = 83, g = 83, b = 210},
	babybox = false,
})

ranks.register("developer", {
	prefix = "Developer",
	colour = {a = 255, r = 140, g = 98, b = 52},
	babybox = false,
})

ranks.register("ranger", {
	prefix = "Ranger",
	colour = {a = 255, r = 180, g = 132, b = 0},
	babybox = false,
})

ranks.register("honoree", {
	prefix = "Honoree",
	colour = {a = 255, r = 205, g = 144, b = 112},
	babybox = false,
})

ranks.register("established", {
	prefix = "Regular",
	colour = {a = 255, r = 90, g = 180, b = 120},
	babybox = false,
})

ranks.register("o_O", {
	prefix = "o_O",
	colour = {a = 255, r = 180, g = 180, b = 158},
	babybox = true,
})

ranks.register("O_O", {
	prefix = "O_O",
	colour = {a = 255, r = 238, g = 130, b = 238},
	babybox = false,
})

